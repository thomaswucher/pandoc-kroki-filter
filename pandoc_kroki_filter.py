#!/usr/bin/env python

import os
import base64
import zlib
import urllib.request
import tempfile
import subprocess

from panflute import run_filter, CodeBlock, Image, Figure, Plain, Str, Caption

# Diagram types that will be supported.
DIAGRAM_TYPES = [
    "actdiag",
    "bpmn",
    "pikchr",
    "nwdiag",
    "c4plantuml",
    "rackdiag",
    "dot",
    "symbolator",
    "d2",
    "tikz",
    "mermaid",
    "erd",
    "graphviz",
    "vegalite",
    "ditaa",
    "umlet",
    "diagramsnet",
    "plantuml",
    "seqdiag",
    "nomnoml",
    "wavedrom",
    "structurizr",
    "bytefield",
    "wireviz",
    "excalidraw",
    "dbml",
    "packetdiag",
    "svgbob",
    "vega",
    "blockdiag",
]
DIAGRAM_SYNONYMNS = {"c4": "c4plantuml"}
AVAILABLE_DIAGRAMS = DIAGRAM_TYPES + list(DIAGRAM_SYNONYMNS.keys())

# List of diagrams types the user chooses not to process
DIAGRAM_BLACKLIST = list(
    filter(
        lambda d: d in AVAILABLE_DIAGRAMS,
        os.environ.get("KROKI_DIAGRAM_BLACKLIST", "").split(","),
    )
)

# kroki server to point to
KROKI_SERVER = os.environ.get("KROKI_SERVER", "https://kroki.io/")
KROKI_SERVER = KROKI_SERVER[:-1] if KROKI_SERVER[-1] == "/" else KROKI_SERVER


def kroki(elem, doc):
    if type(elem) == CodeBlock:
        diagram_classes = list(set(AVAILABLE_DIAGRAMS) & set(elem.classes))
        if len(diagram_classes) == 1 and diagram_classes[0] not in DIAGRAM_BLACKLIST:
            # find the correct diagram type to use with kroki
            if diagram_classes[0] in DIAGRAM_SYNONYMNS.keys():
                diagram_type = DIAGRAM_SYNONYMNS[diagram_classes[0]]
            else:
                diagram_type = diagram_classes[0]

            # create the url to the kroki diagram and link as an image
            encoded = base64.urlsafe_b64encode(
                zlib.compress(elem.text.encode("utf-8"), 9)
            ).decode()

            if diagram_type == "tikz":
                url = f"{KROKI_SERVER}/{diagram_type}/pdf/{encoded}"

                image_file_name = tempfile.NamedTemporaryFile().name
                urllib.request.urlretrieve(url, image_file_name + ".pdf")
                subprocess.check_output(["pdfcrop", image_file_name + ".pdf"])

                image = Image(Str(""), url=image_file_name + "-crop.pdf")
            else:
                image = Image(
                    Str(""),
                    url=f"{KROKI_SERVER}/{diagram_type}/svg/{encoded}",
                    attributes={"width": elem.attributes.get("width", "")},
                )

            caption = Caption(Plain(Str(elem.attributes.get("caption", ""))))
            figure = Figure(Plain(image), caption=caption, identifier=elem.identifier)

            return figure


def main(doc=None):
    return run_filter(kroki, doc=doc)


if __name__ == "__main__":
    main()
